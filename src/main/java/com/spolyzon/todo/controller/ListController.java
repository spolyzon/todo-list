package com.spolyzon.todo.controller;

import com.spolyzon.todo.domain.entity.TodoList;
import com.spolyzon.todo.domain.repository.TodoListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/lists")
@SuppressWarnings("unused")
public class ListController {

    @Autowired
    private TodoListRepository repository;

    @GetMapping
    public TodoList getTodoLists() {
        return repository.findById("list1").orElseThrow(RuntimeException::new);
    }

}
