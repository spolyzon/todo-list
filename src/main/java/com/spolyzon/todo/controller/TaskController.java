package com.spolyzon.todo.controller;

import com.spolyzon.todo.domain.entity.Task;
import com.spolyzon.todo.service.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/tasks")
@SuppressWarnings("unused")
public class TaskController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    private TaskService taskService;

    @GetMapping
    public List<Task> getAllTasks() {
        LOGGER.info("Retrieving all Tasks.");
        return taskService.getAllTasks();
    }

    @GetMapping("/{id}")
    public Task getTask(@PathVariable("id") String taskId) {
        LOGGER.info("Getting a Task with id: {}.", taskId);
        throw new UnsupportedOperationException("Not yet supported");
    }

    @PostMapping
    public ResponseEntity<Task> createTask(@RequestBody Task task) {
        LOGGER.info("Creating new Task.");
        throw new UnsupportedOperationException("Not yet supported");
    }

    @PutMapping("/{id}")
    public ResponseEntity<Task> updateTask(@PathVariable("id") String taskId) {
        LOGGER.info("Updating a Task with id: {}.", taskId);
        throw new UnsupportedOperationException("Not yet supported");
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTask(@PathVariable("id") String taskId) {
        LOGGER.info("Deleting a Task with id: {}.", taskId);
        throw new UnsupportedOperationException("Not yet supported");
    }

}
