package com.spolyzon.todo.exception;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@SuppressWarnings("unused")
public class TaskExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<Message> handleException(Throwable thr) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Message(thr.getMessage()));
    }

    @Setter
    @Getter
    @AllArgsConstructor
    @NoArgsConstructor
    @EqualsAndHashCode
    @ToString
    private static class Message {
        private String message;
    }
}
