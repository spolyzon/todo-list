package com.spolyzon.todo.domain.repository;

import com.spolyzon.todo.domain.entity.TodoList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TodoListRepository extends JpaRepository<TodoList, String> {
}
