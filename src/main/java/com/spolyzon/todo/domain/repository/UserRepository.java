package com.spolyzon.todo.domain.repository;

import com.spolyzon.todo.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@SuppressWarnings("unused")
public interface UserRepository extends JpaRepository<User, String> {
}
