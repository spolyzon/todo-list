package com.spolyzon.todo.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "TODO_LIST")
public class TodoList {

    @Id
    @Column(name = "LIST_ID")
    private String id;

    @OneToMany(mappedBy = "list", fetch = FetchType.EAGER)
    private List<Task> tasks;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "USERNAME")
    private User user;
}
