package com.spolyzon.todo.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "TASK")
public class Task {

    @Id
    @Column(name = "TASK_ID", nullable = false)
    private String id;

    @Column(name = "TASK_NAME", nullable = false)
    private String name;

    @Column(name = "TASK_DESCRIPTION")
    private String description;

    @Column(name = "TASK_COMPLETED")
    private boolean completed;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "LIST_ID")
    private TodoList list;
}
