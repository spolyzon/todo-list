package com.spolyzon.todo;

import com.spolyzon.todo.domain.entity.Task;
import com.spolyzon.todo.domain.entity.TodoList;
import com.spolyzon.todo.domain.entity.User;
import com.spolyzon.todo.domain.repository.TaskRepository;
import com.spolyzon.todo.domain.repository.TodoListRepository;
import com.spolyzon.todo.domain.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@SpringBootApplication
public class TodoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TodoApplication.class, args);
	}

	@Bean
	CommandLineRunner init(TaskRepository repository, TodoListRepository listRepository, UserRepository userRepository) {
		return x -> {
			User user = new User("spyros", "pass", Collections.emptyList());
			userRepository.save(user);

			TodoList list = new TodoList("list1", Collections.EMPTY_LIST, user);
			listRepository.save(list);

			Task t1 = new Task(UUID.randomUUID().toString(), "task 1", "description 1", false, list);
			Task t2 = new Task(UUID.randomUUID().toString(), "task 2", "description 2", false, list);
			Task t3 = new Task(UUID.randomUUID().toString(), "task 3", "description 3", false, list);
			Task t4 = new Task(UUID.randomUUID().toString(), "task 4", "description 4", false, list);
			Task t5 = new Task(UUID.randomUUID().toString(), "task 5", "description 5", true, list);
			Arrays.asList(t1, t2, t3, t4, t5).forEach(repository::save);
		};
	}

}
